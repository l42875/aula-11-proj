package pt.uevora;
  
import org.junit.After; 
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator; 

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    @Test
    public void testSum() throws Exception {
        Double result = calculator.execute("2+3");

        assertEquals("The sum result of 2 + 3 must be 5",  5D, (Object)result);
    }
    
    @Test
    public void testSub() throws Exception {
        Double result = calculator.execute("2-3");
        
        assertEquals("O resultado de 2-3 dever� ser -1",  -1D, (Object)result);
    }
    
    @Test
    public void testDiv() throws Exception {
        Double result = calculator.execute("10/2");
       
        assertEquals("A divis�o de 10 por 2 dever� ser 5",  5D, (Object)result);
    }
    
    @Test
    public void testPotencia() throws Exception {
        final Double result = calculator.execute("2^2");

        assertEquals("2 elevado a 2 dever� ser 4",4D,(Object)result);
    }
    
    @Test
    public void testMult() throws Exception {
        Double result = calculator.execute("2*3");
        
        assertEquals("The mult result of 2 * 3 must be 6",  6D, (Object)result);
    }
}
